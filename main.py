import socket

from fastapi import FastAPI, HTTPException

from apps.api.urls import api
from core.crawler_bot_worker import CrawlerBotFactory
from core.exception_handler import exception_handler
from core.settings import settings

app = FastAPI()
CrawlerBotFactory.back_up_init(server_id=settings.SERVER_ID)
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=settings.BACKEND_CORS_ORIGINS,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )
app.add_exception_handler(HTTPException, exception_handler)
app.include_router(api, prefix='/api/v1')


# @app.get("/background-tasks")
# async def background_tasks():
#     create_tasks.delay(1, 2, 3)
#     return {"message": "Run background"}

