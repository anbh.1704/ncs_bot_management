import json
import threading

import requests
from kafka import KafkaConsumer
from kafka.admin import KafkaAdminClient, NewTopic

from apps.api.crawler.youtube_crawler.youtube_crawler import YoutubeInputHandler
from apps.models.crawler import CrawlProxy
from apps.models.social import SocialAccount
from apps.repositories.crawler_bot_repository import CrawlerBotRepository
from apps.repositories.crawler_proxy_repository import CrawlerProxyRepository
from apps.repositories.social_account_repository import SocialAccountRepository
from core.custom_error import SocialAccountError
from core.database import db
from core.settings import crawler_logger, settings

KAFKA_SERVER_URL = settings.KAFKA_URL
OFFSET_FILTER = 'earliest'
active_bots = {}
active_threads = {}
stop_consumer_flags = {}


class SocialTool:
    TOOL_OPTION = {
        'YOUTUBE': YoutubeInputHandler,
        'TIKTOK': 'TiktokPseudo',
        'FACEBOOK': 'FacebookPseudo',
    }


social_type_dict = {
    "YOUTUBE": 1,
    "TIKTOK": 2,
    "FACEBOOK": 3,
    "NEWS": 4,
}


class CrawlerBot:

    def __init__(self, topic, group_id, id):
        self.consumer = KafkaConsumer(
            topic,
            group_id=group_id,
            bootstrap_servers=KAFKA_SERVER_URL,
            auto_offset_reset=OFFSET_FILTER,
            enable_auto_commit=True,
            value_deserializer=lambda x: self.deserialize_message(x)
        )
        self.topic = topic
        self.group_id = group_id
        self.id = id
        self.social_network = None
        self.current_script = None
        self.current_params = None

    def deserialize_message(self, value):
        try:
            return json.loads(value.decode('utf-8'))
        except json.JSONDecodeError as e:
            print(f"Error decoding message: {e}")
            return None

    def process_message(self, message):
        UN_CRAWL = True
        self.social_network = message.get('social_network').upper()
        self.current_script = message['mode'].get('id')
        self.current_params = json.dumps(message)
        social_id = social_type_dict[self.social_network]
        input_data = SocialTool.TOOL_OPTION[
            self.social_network].input_message_detech(message)

        current_bot = CrawlerBotRepository().update_current_info(
            self.id,
            social_id,
            self.current_script,
            self.current_params,
            status=1
        )
        current_account: SocialAccount = (SocialAccountRepository().
                                          get_account_by_username(input_data.username))

        if current_account:
            current_account.crawl_bot_id = self.id
            db.commit()

        proxy_info: CrawlProxy = CrawlerProxyRepository().get_free_proxy()
        if proxy_info:
            proxy_info.crawl_bot_id = self.id
        db.commit()
        while UN_CRAWL:
            try:
                SocialTool.TOOL_OPTION[
                    self.social_network].crawl_handle(
                    input_data=input_data,
                    proxy_info=proxy_info
                )
                UN_CRAWL = False
                current_bot.current_script = None
                current_bot.current_params = None
                current_account.crawl_bot_id = None
                db.commit()
            except SocialAccountError as sae:
                if current_account:
                    current_account.state_id = SocialAccount.SocialAccountState.ERROR.value
                    current_account.crawl_bot_id = None
                    db.commit()
                new_account: SocialAccount = (SocialAccountRepository(db).
                                              get_available_account_of_social(social_id))
                if new_account:
                    input_data.username = new_account.username
                    input_data.password = new_account.password
                    new_account.crawl_bot_id = self.id
                    db.commit()
                else:
                    UN_CRAWL = False
                    self.reset_bot_status(current_bot, current_account)
                    crawler_logger.error("There is no available account")
                crawler_logger.error(str(sae))
            except BaseException as e:
                UN_CRAWL = False
                self.reset_bot_status(current_bot, current_account)
                crawler_logger.error(str(e))
                crawler_logger.error_input(message)
            finally:
                if proxy_info:
                    self.reset_proxy(proxy_info)
        current_bot.status = 0
        db.commit()

    def reset_proxy(self, proxy: CrawlProxy):
        proxy.crawl_bot_id = None
        proxy_info = f'{proxy.ip_address}:{proxy.port}'
        request_url = f'{settings.PROXY_SERVER_URL}/reset?proxy={proxy_info}'
        try:
            requests.get(request_url)
        except BaseException as e:
            crawler_logger.error(str(e))
        finally:
            db.commit()

    def reset_bot_status(self, current_bot, current_account=None):
        current_bot.current_script = None
        current_bot.current_params = None

        if current_account:
            current_account.crawl_bot_id = None
        db.commit()

    def run(self):
        # bot = CrawlerBotRepository().get_bot_by_id(self.id)
        # current_params = bot.current_params
        # if current_params:
        #     try:
        #         self.process_message(json.loads(current_params))
        #     except BaseException as e:
        #         crawler_logger.error(str(e))
        for message in self.consumer:
            if stop_consumer_flags.get(self.id):
                break
            try:
                crawler_logger.info(f'Bot {self.id}: {message}')
                self.process_message(message.value)
            except BaseException as e:
                crawler_logger.error(str(e))

    def stop(self):
        self.consumer.close()


class CrawlerBotFactory:
    @staticmethod
    def create_bot(topic, group_id, id):
        crawl_bot = CrawlerBot(topic,
                               group_id,
                               id)
        active_bots[id] = crawl_bot
        stop_consumer_flags[id] = False
        crawl_bot.run()

    @staticmethod
    def start_bot_thread(topic, group_id, id):
        bot_thread = threading.Thread(target=CrawlerBotFactory.create_bot,
                                      args=(topic, group_id, id))
        active_threads[id] = bot_thread
        bot_thread.start()

    @staticmethod
    def create_topic_with_partitions(topic_name, num_partitions=10,
                                     replication_factor=1,
                                     bootstrap_servers=KAFKA_SERVER_URL):
        try:
            admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)
            new_topic = NewTopic(name=topic_name, num_partitions=num_partitions,
                                 replication_factor=replication_factor)
            admin_client.create_topics(new_topics=[new_topic])
            admin_client.close()
            return True
        except Exception as e:
            return False

    @staticmethod
    def back_up_init(server_id):
        bots = CrawlerBotRepository().get_bot_server_id(server_id)

        for bot in bots:
            CrawlerBotFactory.start_bot_thread(bot.topic,
                                               bot.group_id,
                                               bot.id)
