class SocialAccountError(ValueError):
    def __init__(self, username, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.username = username

    def __str__(self):
        return f"Error with account: {self.username}"
