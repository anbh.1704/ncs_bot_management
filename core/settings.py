from dotenv import load_dotenv
from elasticsearch import Elasticsearch
# from pydantic import validator
from pydantic_settings import BaseSettings

from apps.api.crawler.utils.crawler_logger import CrawlerLogger

load_dotenv()


class Settings(BaseSettings):
    # SECRET_KEY: str
    KAFKA_URL: str
    MYSQL_URL: str
    # BACKEND_CORS_ORIGINS: str
    # JWT_SECRET_KEY: str
    # JWT_ALGORITHM: str = 'HS256'
    # JWT_EXPIRATION_DELTA: int
    CELERY_BROKER_URL: str
    REDIS_URL: str
    SERVER_PORT: int
    ELASTIC_SEARCH_URL: str
    PROXY_SERVER_URL: str
    SERVER_ID: int

    # @validator("BACKEND_CORS_ORIGINS")
    # def assemble_cors_origins(cls, v: str):
    #     return [i.strip() for i in v.split(",")]

    class Config:
        env_file = '.env'


settings = Settings()
es_url = settings.ELASTIC_SEARCH_URL.split(":")
es = Elasticsearch([{'host': es_url[0], 'port': int(es_url[1]), 'scheme': 'http'}])
crawler_logger = CrawlerLogger(es)
