import inspect
import logging
from elasticsearch import Elasticsearch
from elasticsearch import helpers


class CrawlerLogger:
    def __init__(self, es: Elasticsearch):
        self.info_logger = self.setup_logger("info_logger", logging.INFO)
        self.error_logger = self.setup_logger("error_logger", logging.ERROR)
        self.es = es

        #Info Termnial Log
        logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")

    def setup_logger(self, logger_name, log_level):
        logger = logging.getLogger(logger_name)
        logger.setLevel(log_level)

        logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

        if logger.hasHandlers():
            logger.handlers.clear()

        return logger

    def info(self, message):
        self.info_logger.info(message)
        self.send_to_elasticsearch('info', message)

    def error(self, message):
        caller_class = inspect.stack()[1][0].f_locals.get("self", None)
        if caller_class:
            class_name = caller_class.__class__.__name__
            error_method = inspect.stack()[1][3]
            formatted_message = f"{class_name}: {error_method} - {message}"
            # self.error_logger.error(formatted_message)
            self.send_to_elasticsearch('error', formatted_message)

    def error_input(self, message):
        caller_class = inspect.stack()[1][0].f_locals.get("self", None)
        if caller_class:
            class_name = caller_class.__class__.__name__
            error_method = inspect.stack()[1][3]
            formatted_message = f"{class_name}: {error_method} - {message}"
            # self.error_logger.error(formatted_message)
            self.send_to_elasticsearch('error_input', formatted_message)

    def send_to_elasticsearch(self, level, message):
        log_entry = {
            'level': level,
            'message': message
        }
        self.es.index(index='crawler_logs', body=log_entry)

    def close(self):
        for handler in self.info_logger.handlers:
            handler.close()
        for handler in self.error_logger.handlers:
            handler.close()
