import time
from typing import Dict

from selenium.webdriver.common.by import By

from apps.api.crawler.kafka_ncs_temp import push_kafka
from core.settings import crawler_logger
from .utils import (
    DetailCrawler,
    YoutubeUtil,
    StringHandler,
    GChromeDriver, InteractOption,
)

YOUTUBE_HOMEPAGE_URL = "https://www.youtube.com"
BROWSER_LANGUAGE = "en"
HASHTAG_SEARCH_URL = "https://www.youtube.com/hashtag"


class YoutubeCrawlerTool:
    def __init__(self, username=None, password=None, proxy=None):
        self.driver_factory = GChromeDriver
        self.driver = self.driver_factory.init_driver(username, password, proxy)
        self.driver.set_window_size(1024, 3000)
        self.detail_crawler = DetailCrawler(self.driver)
        self.string_handler = StringHandler
        self.util = YoutubeUtil(self.driver)
        self.username = username
        self.password = password

    def run_script1(self, interact_option, keyword_list=[]):
        """
        Tìm theo keyword=> Lấy Video link ở màn đó
        => Crawl dữ liệu của từng Video
        """
        crawled_list = []
        for keyword in keyword_list:
            videos_data = self._scrape_videos_by_keyword(keyword, interact_option)
            try:
                push_kafka(posts=videos_data)
                crawled_list.extend(videos_data)
            except Exception as e:
                crawler_logger.error(str(e))
        return crawled_list

    def run_script2(self, interact_option, channel_url_list=[]):
        """
        Crawl video của từng channel
        Input: List chứa các Channel
        """
        crawled_data = []
        for channel_url in channel_url_list:
            try:
                videos_data = self._scrape_videos_by_channel(channel_url,
                                                             interact_option)
                push_kafka(posts=videos_data)
                crawled_data.extend(videos_data)
            except Exception as e:
                crawler_logger.error((str(e)))

        return crawled_data

    def run_script3(self, interact_option, hashtag_list=[]):
        """
        Crawl toàn bộ video theo list Hashtag
        """
        crawled_data = []
        for hashtag in hashtag_list:
            videos_data = self._scrape_videos_by_hashtag(hashtag,
                                                         interact_option)
            push_kafka(posts=videos_data)
            crawled_data.extend(videos_data)
        return crawled_data

    def channel_url_scrape_video(self, video_links, channel_url):
        data_by_channel_dict = {channel_url: []}
        for index, video_link in enumerate(video_links):
            try:
                data_by_channel_dict[channel_url].append(
                    self.detail_crawler.run(video_link)
                )
                crawler_logger.info(
                    f"Processing keyword: {channel_url} ({index + 1}/{len(video_links)})"
                )
            except Exception as e:
                crawler_logger.error(str(e))
        return data_by_channel_dict

    def scrape_video_by_links(self, interact_option, video_links, keyword=None):
        crawled_data_list = []
        self.driver.switch_to.window(self.driver.window_handles[1])
        for index, video_link in enumerate(video_links):
            try:
                crawl_data = self.detail_crawler.run(
                    interact_option=interact_option,
                    video_url=video_link,
                    keyword=keyword
                )
                if crawl_data:
                    video_detail, comments = crawl_data
                    crawled_data_list.append(video_detail)
                    crawled_data_list.extend(comments)
            except Exception as e:
                crawler_logger.error(str(e))
        self.driver.switch_to.window(self.driver.window_handles[0])
        return crawled_data_list

    def report_video(self, video_link):
        self.driver.get(video_link)
        time.sleep(2)
        self.util.report()

    def _scrape_videos_by_keyword(self, keyword, interact_option):
        format_keyword = "+".join(keyword.split())
        search_url = f"https://www.youtube.com/results?search_query={format_keyword}"
        self.driver.get(search_url)
        self.util.recently_click()
        return self._crawled_data_in_search_screen(interact_option,
                                                   keyword)

    def _scrape_videos_by_channel(self, channel_url,
                                  interact_option):
        self.driver.get(channel_url)
        time.sleep(3)
        if self.username and self.password:
            try:
                self.util.subscribe_channel()
            except Exception as e:
                crawler_logger.error(str(e))

        self.util.video_click()
        return self._crawled_data_in_thumbnails_screen(interact_option)

    def _crawled_data_in_search_screen(self, interact_option, keyword):
        crawled_rows_text = []
        crawled_data_list = []
        crawled_video_links = []
        can_scroll = True

        while can_scroll:
            video_links = self.extract_link_from_page()
            prepare_video_links = {
                video_link for video_link in video_links
                if video_links not in crawled_video_links
            }

            if len(prepare_video_links) == 0:
                can_scroll = False
                continue

            try:
                crawled_data_list.extend(self.scrape_video_by_links(
                    interact_option=interact_option,
                    video_links=list(prepare_video_links),
                    keyword=keyword
                ))
                crawled_video_links.extend(prepare_video_links)
                prepare_video_links.clear()
            except Exception as e:
                crawler_logger.error((str(e)))
            crawled_rows_text.extend(prepare_video_links)
            self.detail_crawler.scroll_down_action(4)
            time.sleep(5)
        return crawled_data_list

    def _crawled_data_in_thumbnails_screen(self, interact_option):
        crawled_rows_text = []
        prepare_video_links = set()
        crawled_data_list = []
        crawled_video_links = []
        can_scroll = True

        while can_scroll:
            rows = self.driver.find_elements(
                By.XPATH, '//*[@id="contents"]/ytd-rich-grid-row'
            )
            new_rows = [row for row in rows if row.text not in crawled_rows_text]

            if len(new_rows) == 0:
                can_scroll = False
                continue

            for index, row in enumerate(new_rows):
                thumbnails = row.find_elements(
                    By.CSS_SELECTOR, ".style-scope .ytd-rich-item-renderer"
                )
                for thumbnail in thumbnails:
                    links = thumbnail.find_elements(By.CSS_SELECTOR, "a#thumbnail")
                    video_links = {
                        link.get_attribute("href")
                        for link in links
                        if (link.get_attribute("href") not in crawled_video_links
                            and 'watch?' in link.get_attribute("href"))
                    }
                    prepare_video_links.update(video_links)
                if (len(prepare_video_links) > 10
                        or index == len(new_rows) - 1):
                    try:
                        crawled_data_list.extend(self.scrape_video_by_links(
                            interact_option=interact_option,
                            video_links=list(prepare_video_links),
                        ))
                        crawled_video_links.extend(prepare_video_links)
                        prepare_video_links.clear()
                    except Exception as e:
                        crawler_logger.error((str(e)))
                crawled_rows_text.append(row.text)
            self.detail_crawler.scroll_down_action(4)
            time.sleep(5)
        return crawled_data_list

    def _scrape_videos_by_hashtag(self, hashtag, interact_option):
        self.driver.get(f"{HASHTAG_SEARCH_URL}/{hashtag}")
        return self._crawled_data_in_thumbnails_screen(interact_option)

    def extract_link_from_page(self):
        video_links_xpath = (
            '//a[contains(@href, "/watch?v=")]'
        )
        video_links_info = self.driver.find_elements(By.XPATH, video_links_xpath)
        video_links = {link.get_attribute("href") for link in video_links_info}
        return list(video_links)


class YoutubeCrawlInput:
    def __init__(self, **kwargs):
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        self.run_script = kwargs.get('run_script')
        self.script_params = kwargs.get('script_params', [])
        self.interact_option = kwargs.get('interact_option', [])
        self.comments_text = kwargs.get('comments_text', [])

    def interact_option_extract(self):
        self.interact_option = [option.upper() for option
                                in self.interact_option]
        interact_option = InteractOption(
            like_mode="LIKE" in self.interact_option,
            report_mode="REPORT" in self.interact_option,
            comment_mode="COMMENT" in self.interact_option,
            comment_sample_list=self.comments_text,
        )
        return interact_option


class YoutubeCrawlerJob:
    def __init__(self):
        pass

    def __getitem__(self, key):
        if hasattr(self, key) and callable(getattr(self, key)):
            return lambda *args, **kwargs: getattr(self, key)(*args, **kwargs)
        else:
            raise KeyError(f"Method '{key}' not found")

    @staticmethod
    def run_script1(interact_option, username=None, password=None, keywords=[], proxy=None):
        tool = YoutubeCrawlerTool(username=username, password=password, proxy=proxy)
        tool.run_script1(keyword_list=keywords, interact_option=interact_option)
        tool.driver.quit()

    @staticmethod
    def run_script2(interact_option, username, password, channel_urls, proxy=None):
        tool = YoutubeCrawlerTool(username=username, password=password, proxy=proxy)
        tool.run_script2(interact_option=interact_option,
                         channel_url_list=channel_urls)
        tool.driver.quit()

    @staticmethod
    def run_script3(interact_option, username, password, hashtag_list, proxy=None):
        tool = YoutubeCrawlerTool(username=username, password=password, proxy=proxy)
        tool.run_script3(hashtag_list=hashtag_list, interact_option=interact_option)
        tool.driver.quit()


class YoutubeInputHandler:
    @staticmethod
    def input_message_detech(input_message: Dict):
        youtube_input_dict = {}
        if input_message.get("account"):
            youtube_input_dict['username'] = input_message['account'].get('username')
            youtube_input_dict['password'] = input_message['account'].get('password')

        youtube_input_dict['run_script'] = input_message['mode'].get('id')
        youtube_input_dict['script_params'] = input_message['mode'].get('script_params')
        youtube_input_dict['interact_option'] = input_message['mode'].get('type_auto')
        youtube_input_dict['comments_text'] = input_message['mode'].get('comments_text')
        return YoutubeCrawlInput(**youtube_input_dict)

    @staticmethod
    def crawl_handle(input_data: YoutubeCrawlInput, proxy_info):
        mapping_script = {
            1: YoutubeCrawlerJob.run_script1,
            2: YoutubeCrawlerJob.run_script2,
            3: YoutubeCrawlerJob.run_script3
        }

        mapping_script[input_data.run_script](
            input_data.interact_option_extract(),
            input_data.username,
            input_data.password,
            input_data.script_params,
            proxy=proxy_info
        )
