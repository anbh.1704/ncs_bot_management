class Post:
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.type = kwargs.get("type")
        self.time_crawl = kwargs.get("time_crawl")
        self.link = kwargs.get("link")
        self.author = kwargs.get("author")
        self.author_link = kwargs.get("author_link")
        self.avatar = kwargs.get("avatar")
        self.created_time = kwargs.get("created_time")
        self.content = kwargs.get("content")
        self.image_url = kwargs.get("image_url")
        self.like = kwargs.get("like")
        self.comment = kwargs.get("comment")
        # self.haha = kwargs.get("")
        # self.wow = kwargs.get("")
        # self.sad = kwargs.get("")
        # self.love = kwargs.get("")
        # self.angry = kwargs.get("")
        # self.share = kwargs.get("")
        self.domain = kwargs.get("domain")
        self.hashtag = kwargs.get("extract_hashtags") or []
        # self.music = kwargs.get("")
        self.title = kwargs.get("title")
        self.duration = kwargs.get("duration")
        self.view = kwargs.get("view")
        self.description = kwargs.get("description")
        self.video = kwargs.get("video")
        self.source_id = kwargs.get("source_id")

    # Hàm xác định xem post được crawl đủ hay chưa
    def is_valid(self) -> bool:
        is_valid = (
                self.id != ""
                and self.author != ""
                and self.link != ""
                and self.created_time
        )
        return is_valid

    def __str__(self) -> str:
        string = ""
        for attr_name, attr_value in self.__dict__.items():
            string = f"{attr_name}={attr_value}\n" + string
        return string

    def is_valid(self) -> bool:
        is_valid = self.id != "" and self.author != "" and self.link != "" and self.created_time
        return is_valid
