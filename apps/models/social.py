from enum import Enum

from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql import func

Base = declarative_base()


class SocialAccount(Base):
    __tablename__ = 'social_account'

    class SocialAccountState(Enum):
        NORMAL = 1
        ERROR = 0

    id = Column(Integer, primary_key=True)
    username = Column(String(63), unique=True, nullable=True)
    password = Column(String(20))
    email = Column(String(63), unique=True, nullable=True)
    type = Column(String)
    status = Column(Integer, default=SocialAccountState.NORMAL.value)
    twofa = Column(String(500), nullable=True)
    profile_id = Column(Integer, nullable=True)
    is_recovery = Column(Boolean, default=False)
    recovery_account_id = Column(Integer, nullable=True)
    crawl_bot_id = Column(Integer, nullable=True)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, onupdate=func.now())
