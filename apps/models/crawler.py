from datetime import datetime
from enum import Enum as PyEnum

from sqlalchemy import Column, Integer, String, DateTime, func

from core.database import Base, engine



class CrawlerBot(Base):
    __tablename__ = "crawl_bot"

    class Social(PyEnum):
        YOUTUBE = 1
        TIKTOK = 2
        FACEBOOK = 3

    class Status(PyEnum):
        RUNNING = 1
        WAITING = 0

    id = Column(Integer, primary_key=True, autoincrement=True)
    topic = Column(String(255))
    group_id = Column(String(255), unique=True)
    status = Column(Integer, default=Status.WAITING.value)
    social_type = Column(Integer, default=None, nullable=True)
    current_script = Column(Integer, nullable=True)
    current_params = Column(String, nullable=True)
    created_at = Column(DateTime, default=datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    server_id = Column(Integer)


class CrawlProxy(Base):
    __tablename__ = "crawl_proxy"

    id = Column(Integer, primary_key=True, autoincrement=True)
    ip_address = Column(String(255))
    port = Column(Integer, default=None, nullable=True)
    status = Column(Integer, default=None, nullable=True)
    username = Column(Integer, default=None, nullable=True)
    password = Column(Integer, default=None, nullable=True)
    crawl_bot_id = Column(Integer, default=None, nullable=True)
    created_at = Column(DateTime, default=datetime.utcnow, server_default=func.now())
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


Base.metadata.create_all(engine)
