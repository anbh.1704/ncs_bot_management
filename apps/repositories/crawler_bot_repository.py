from sqlalchemy import func
from sqlalchemy.exc import IntegrityError, SQLAlchemyError, PendingRollbackError

from apps.models.crawler import CrawlerBot
from core.database import SessionLocal


class CrawlerBotRepository:
    def __init__(self):
        self.db = SessionLocal()

    def create_bot(self, topic: str, group_id: str, status: int,
                   server_id: int, social_type: int = None):
        bot = CrawlerBot(topic=topic, group_id=group_id,
                         status=status,
                         server_id=server_id,
                         social_type=social_type)
        try:
            self.db.add(bot)
            self.db.commit()
        except IntegrityError as e:
            self.db.rollback()  # Rollback the invalid transaction
            return None
        except (SQLAlchemyError, PendingRollbackError) as e:
            self.db.rollback()  # Rollback for SQLAlchemy errors, including PendingRollbackError
            return None
        finally:
            self.db.refresh(bot)
            self.db.close()
        return bot

    def get_bot_by_id(self, bot_id: int):
        return self.db.query(CrawlerBot).filter(CrawlerBot.id == bot_id).first()

    def get_bot_by_group_id(self, group_id: str):
        return self.db.query(CrawlerBot).filter(CrawlerBot.group_id == group_id).first()

    def get_all_bots(self):
        return self.db.query(CrawlerBot).all()

    def check_exist_group_id(self, group_id):
        return self.db.query(func.count()).filter(CrawlerBot.group_id == group_id).scalar()

    def check_exist_id(self, id):
        return self.db.query(func.count()).filter(CrawlerBot.id == id).scalar()

    def update_bot_status(self, bot_id: int, new_status: int):
        try:
            bot = self.get_bot_by_id(bot_id)
            if bot:
                bot.status = new_status
                self.db.commit()
                self.db.refresh(bot)
                return bot
            return None
        except IntegrityError as e:
            self.db.rollback()
            return None
        finally:
            self.db.close()

    def delete_bot(self, bot_id: int):
        bot = None
        try:
            bot = self.get_bot_by_id(bot_id)
            if bot:
                self.db.delete(bot)
                self.db.commit()
        except IntegrityError as e:
            self.db.rollback()  # Rollback the invalid transaction
        except (SQLAlchemyError, PendingRollbackError) as e:
            self.db.rollback()  # Rollback for SQLAlchemy errors, including PendingRollbackError
        finally:
            self.db.refresh(bot)
            self.db.close()
        return bot

    def get_bot_server_id(self, server_id):
        return self.db.query(CrawlerBot).filter(CrawlerBot.server_id == server_id).all()

    def update_current_info(self, id, current_social_network, current_script, current_params, status):
        try:
            bot = self.db.query(CrawlerBot).filter(CrawlerBot.id == id).first()
            if bot:
                bot.social_type = current_social_network
                bot.current_script = current_script
                bot.current_params = current_params
                bot.status = status
                self.db.commit()
                self.db.refresh(bot)
                return bot
        except IntegrityError as e:
            self.db.rollback()
        except (SQLAlchemyError, PendingRollbackError) as e:
            self.db.rollback()
        finally:
            self.db.close()
