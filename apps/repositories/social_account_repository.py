from sqlalchemy.exc import IntegrityError, SQLAlchemyError, PendingRollbackError
from sqlalchemy.orm import Session

from apps.models.social import SocialAccount
from core.database import SessionLocal


class SocialAccountRepository:
    def __init__(self):
        self.db = SessionLocal()

    def create_account(self, username: str, password: str, email: str, type_id: int,
                       state_id: int, access_token: str, profiles: str, is_recovery: bool,
                       recovery_account_id: int, crawl_bot_id: int):
        account = SocialAccount(username=username, password=password,
                                email=email, type_id=type_id, state_id=state_id,
                                access_token=access_token, profiles=profiles, is_recovery=is_recovery,
                                recovery_account_id=recovery_account_id, crawl_bot_id=crawl_bot_id)
        try:
            self.db.add(account)
            self.db.commit()
            self.db.refresh(account)
        except IntegrityError as e:
            self.db.rollback()  # Rollback the invalid transaction
            return None
        except (SQLAlchemyError, PendingRollbackError) as e:
            self.db.rollback()  # Rollback for SQLAlchemy errors, including PendingRollbackError
            # Log the error or handle it as needed
            return None
        finally:
            self.db.close()
        return account

    def get_account_by_username(self, username: str):
        return self.db.query(SocialAccount).filter(
            SocialAccount.username == username,
        ).first()

    def get_available_account_of_social(self, type):
        return self.db.query(SocialAccount).filter(
            SocialAccount.type == type,
            SocialAccount.crawl_bot_id == None,
            SocialAccount.status == SocialAccount.SocialAccountState.NORMAL.value,
        ).first()
