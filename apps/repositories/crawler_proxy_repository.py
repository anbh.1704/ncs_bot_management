from sqlalchemy.orm import Session

from apps.models.crawler import CrawlProxy
from core.database import SessionLocal


class CrawlerProxyRepository:
    def __init__(self):
        self.db = SessionLocal()

    def get_free_proxy(self):
        return self.db.query(CrawlProxy).filter(
            CrawlProxy.crawl_bot_id == None,
            CrawlProxy.status == 1
        ).first()
