# Fastapi python

--- 
Requirements:
- python 3.9+
---

## Start project
### Run for dev
```
python3 -m venv venv
source venv/bin/activate 
pip install -r requirements.txt
uvicorn apps.main:app --reload --port 8008 
```
- Run worker
```
celery -A core.celery worker --loglevel=info
```
- Run beat
```
celery -A core.celery beat --loglevel=info
```
### Run docker
```
docker-compose up --build -d
```